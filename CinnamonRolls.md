# Vegane Zimtschnecken

## Zutaten
### Teig
* 1 kg Mehl
* 450 ml lauwarme Pflanzenmilch
* 180 g Zucker
* 42g (1 Würfel) frische Hefe
* 150g geschmolzene Magerine

### Füllung
* 50g geschmolzene Magerine
* 250g Zucker
* 20g Speisestärke
+ 20g Agavendicksaft/-sirup (oder einen anderen Zuckersirup)
* Zimt nach Geschmack (ca 1-4 EL)

### Glasur
* 250g Puderzucker
* Zitronensaft 
* (lauwarmes Wasser)

## Zubereitung

### Teig
Die Pflanzenmilch erwärmen bis sie ca. handwarm ist. Etwa 30g Zucker und zur Milch hinzugeben und dann den Würfel (wer die Gehzeiten verkürzen möchte kann auch 1,5 Würfel nehmen) Hefe hineinbröseln, nun gut umrühren und kurz stehen lassen. Derweil die Magerine schmelzen und Mehl mit dem restlichen Zucker vermengen. Den Pflanzenmilch-Hefe-Mix zur Mehl Mischung geben und anfangen zu kneten. Wenn sich ein Teig anfängt zu bilden die mittlerweile etwas abgekühlte Magerine hinzugeben und 10 Minuten weiter kneten. Durch das lange Kneten kann das Mehl die gesammte Flüssigkeit aufnehmen und der Teig wird sehr elastisch.
Den Teig zu einer Kugel formen und in eine Schüssel geben, mit einem feuchten Tuch abdecken und an einem warmen Ort etwa 2 Stunden ruhen lassen.

### Füllen
Während der Teig ruht, die Magerine schmelzen und mit den restlichen Zutaten (Zucker, Zimt, Speisestärke und Agavendicksaft) vermengen.

Hat sich der Teig etwa verdoppelt, so ist er bereit ausgerollt zu werden. Den Teig zu einem Rechteck auf eine Dicke von etwa 0,5 cm ausrollen, die kurze Seite sollte ca 25 cm lang sein. Dann die Füllung darauf verteilen, dabei eine lange Kante ca 3 cm frei lassen.
Von der langen Seite (die nicht freigelassen wurde) her eng aufrollen und die Rolle in 3-4 cm dicke Scheiben schneiden. Die Scheiben auf einer Schnittkante auf ein mit Backpapier ausgelegtes Backblech  legen. Circa 4cm zwischen den Schnecken Platz lassen, damit sie noch aufgehen können. Die fertigen Schnecken vor dem Backen 1,5h an einem warmen Ort gehen lassen. 

### Backen
Die vollen Bleche in den kalten Ofen schieben und diesen dann auf 170 Grad Umluft stellen. Nach ca 25 Minuten sollten die Schnecken fertig sein.

### Glasieren
Puderzucker mit etwas Zitronensaft (und damit es nicht zu sauer wird zusätzlich lauwarmem Wasser) verrühren und über die noch warmen Schnecken geben. 

### Genießen!

#### Tipps
* Ich benutze gerne Rohrohrzucker für Teig und Füllung. Meiner Meinung nach schmeckt das etwas vollmundiger.
* Der Agavendicksaft kann mit einem anderen dickflüssigen Zuckersirup ersetzt werden, beispielsweise Zuckerrübensirup. Ahornsirup funktioniert auch, hat einen stärkeren Eigengeschmack und ist flüssiger weshalb ich bei der Verwendung 30g Speisestärke empfehle.
* Die Schnecken lassen sich super einfrieren. Nach dem Backen vollständig auskühlen lassen, vereinzeln, abpacken (ich verwende Gefrierbeutel mit Verschluss) und einfrieren. Zum Verzehr aus dem Tiefkühler holen und einzeln bei Raumtemperatur ca eine Stunde auftauen lassen. Bei Heißhunger; Schnecken gefroren bei höchster Stufe für eine Minute in der Microwelle auftauen.
* Für ein schöneres Schnittbild ungewachste und geschmacksfreie Zahnseide um die Teigrolle legen und eng zuziehen. Damit kommt von alle Seiten gleich viel Druck und die Schnecken bleiben rund.