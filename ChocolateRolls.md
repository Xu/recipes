# Vegane Schokoschnecken

## Zutaten
### Teig (hell)
* 1kg Mehl
* 450ml lauwarme Pflanzenmilch
* 180g (Rohr-)Zucker
* 42g (1 Würfel) frische Hefe
* 150g geschmolzene Magerine

#### Teig (dunkel)
* 10ml Pflanzenmilch
* 2EL Kakao
* 1EL Zucker

### Füllung
* 50g geschmolzene Magerine
* 250g (Rohr-)Zucker
* 20g Speisestärke
+ 20g Agavendicksaft/-sirup
* 400g Schokolade

### Glasur
* 250g Puderzucker
* Zitronensaft 
* (lauwarmes Wasser)

## Zubereitung

### Teig
Die 450ml Pflanzenmilch erwärmen bis sie ca. handwarm ist. Etwa 30g Zucker und zur Milch hinzugeben und dann den Würfel (wer die Gehzeiten verkürzen möchte kann auch 1,5 Würfel nehmen) Hefe hineinbröseln, nun gut umrühren und kurz stehen lassen. Derweil die Magerine schmelzen und Mehl mit dem restlichen Zucker vermengen. Den Pflanzenmilch-Hefe-Mix zur Mehl Mischung geben und anfangen zu kneten. Wenn sich ein Teig anfängt zu bilden die mittlerweile etwas abgekühlte Magerine hinzugeben und 10 Minuten weiter kneten. Durch das lange Kneten kann das Mehl die gesammte Flüssigkeit aufnehmen und der Teig wird sehr elastisch.
Die Hälfte des Teigs abnehmen und mit Pflanzenmilch(10ml), Kakao und Zucker(1EL) verkneten.
Beine Hälften zu einer Kugel formen und in seperate Schüsseln geben, jeweils mit einem feuchten Tuch abdecken und an einem warmen Ort etwa 2 Stunden ruhen lassen.

### Füllen
Während der Teig ruht, die Magerine schmelzen und mit Zucker, Speisestärke und Agavendicksaft vermengen. Schokolade in ca 1 x 1 cm Stücke teilen (alternativ Schoko Chunks oder ähnliches verwenden).

Haben sich die Teige etwa verdoppelt, sind sie bereit ausgerollt zu werden. Die beiden Teige zu deinem kleinen Quadrat formen. Den hellen Teig auf den dunklen Teig Legen und zusammen zu einem Rechteck auf eine Dicke von etwa 0,5 cm ausrollen, die kurze Seite sollte ca 25 cm lang sein. Dann die Füllung darauf verteilen, dabei eine lange Kante ca 3 cm frei lassen. Auf der Füllung die Schokolade verteilen.
Von der langen Seite (die nicht freigelassen wurde) her eng aufrollen und die Rolle in 3-4cm dicke Scheiben schneiden. Die Scheiben auf einer Schnittkante auf ein mit Backpapier ausgelegtes Backblech  legen. Circa 4cm zwischen den Schnecken Platz lassen, damit sie noch aufgehen können. Die fertigen Schnecken vor dem Backen 1,5h an einem warmen Ort gehen lassen. 

### Backen
Die vollen Bleche in den kalten Ofen schieben und diesen dann auf 170 Grad Umluft stellen. Nach ca 25 Minuten sollten die Schnecken fertig sein.

### Glasieren
Puderzucker mit etwas Zitronensaft (und damit es nicht zu sauer wird zusätzlich lauwarmem Wasser) verrühren und über die noch warmen Schnecken geben. 

### Genießen!

#### Tipps
* Ich benutze gerne Rohrohrzucker für Teig und Füllung. Meiner Meinung nach schmeckt das Ergebnis damit etwas vollmundiger.
* Als Schokolade nutze ich von Callebaut einen Mix aus Edelbitter 70,4% Kakao und Callebaut Zartbitter 54,5% Kakao Callets.
* Ich empfehle die Verwendung von hochwertigem Kakao, keine Trinkschokolade.
* Der Agavendicksaft kann mit einem anderen dickflüssigen Zuckersirup ersetzt werden, beispielsweise Zuckerrübensirup. Ahornsirup funktioniert auch, hat einen stärkeren Eigengeschmack und ist flüssiger weshalb ich bei der Verwendung 30g Speisestärke empfehle.
* Die Schnecken lassen sich super einfrieren. Nach dem Backen vollständig auskühlen lassen, vereinzeln, abpacken (ich verwende Gefrierbeutel mit Verschluss) und einfrieren. Zum Verzehr aus dem Tiefkühler holen und einzeln bei Raumtemperatur ca eine Stunde auftauen lassen. Bei Heißhunger; Schnecken gefroren bei höchster Stufe für eine Minute in der Microwelle auftauen.
* Für ein schöneres Schnittbild ungewachste und geschmacksfreie Zahnseide um die Teigrolle legen und eng zuziehen. Damit kommt von alle Seiten gleich viel Druck und die Schnecken bleiben rund.