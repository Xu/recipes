# Double Chocolate Cookies

## Ingredients
* 230g unsalted vegan cold butter
* 160g granulated sugar
* 160g (light) brown sugar
* 430g flour (Weizen Typ 550, all purpose)
* 70g cocoa powder
* 12g salt
* 14g baking soda
* 4g baking powder
* 450g chocolate chips (I use 50% and 70% cocoa)
* optional 5g vanilla extract
* 12g egg substitute (adjacent to 2.5 eggs or 2 whole eggs and 1 egg yolk)
* 15g plantbased milk (I use unsweetened oat milk)

## Directions
* Place the flour, cocoa powder, baking powder, baking soda and salt in a bowl and mix to combine. Set aside.
* Prepare your egg subsitute according to the instructions written on the package (for me, mixing it with water).
* Cut cold vegan butter into ca 1cm cubes. 
* In the bowl of a stand mixer with the paddle attachment, beat the butter for a few seconds on medium speed. Just to break up the cubes.
* Add the sugars and beat for another 30 seconds.
* Add the dry ingredients and beat until there are no big butter chunks left, for me it took about a minute. Small bits of butter are totally fine.
* Add chocolate chips and beat on low speed until mixed through, about 20 seconds.
* Add your prepared egg substitue and optionally vanilla extract. Mix until incorporated.
* Add plant milk while mixing until the dough comes together. The dough should not be wet, add just enough milk until the dough holds together when pressed. 

Weigh each cookie dough ball to an amount of your choice, I do 80g. For not dome shaped cookies press the balls to small disks with a thickness of 1-2cm. Chill in the fridge for at least 3 hours or freeze for at least 1 hour.   
When ready to bake, preheat the oven to 180°C (400°F) and line a baking tray with baking paper. Place cookies onto the tray at least 2cm apart and bake for 18-22 minutes. Since the cookies are dark there will be no visible browning on them.   
Allow to cool for 10-15 minutes before enjoying.   
(Try not to be impatient, the cookies need this time to firm up at room temperature!)

Tipp: Prepare the dough in advance and bake of when ever you want cookies. The dough balls can be stored in the freezer for up to 3 month.  
